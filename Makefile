.PHONY: build clean dev

build: clean
	hugo -F --minify

clean:
	rm -rf public/

dev:
	hugo -D -F server
