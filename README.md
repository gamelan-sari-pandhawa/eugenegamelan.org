# Gamelan Sari Pandhawa Hugo Site

A simple theme with minimal external dependency and [no JavaScript](https://www.youtube.com/watch?v=iYpl0QVCr6U&t=119s). Extends the [Terrassa](https://github.com/danielkvist/hugo-terrassa-theme) theme.

Notable differences include:

* Removal of fontawesome bloat in lieu of Unicode/SVG
* Removal of JavaScript/most animations to make a more pleasant browsing experience
* Lots of little CSS tweaks to change the styling into something more agreeable to the author

Unless otherwise specified, all content on this website are © Gamelan Sari
Pandhawa. Some media have exceptions; File metadata will specify their
individual licenses (if any). While it would be great to license everything
under [CC-BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/) this will
require some effort to complete. A number of resources made for this website
are licensed under Creative Commons. Anything resembling code is under the
[MIT license](LICENSE).

## How to work on the site

### Setup

First, be sure that the theme submodule is cloned in addition to this repository:

```sh
$ git submodule update --init
```

This should clone the submodule into *themes/terrassa/*.

This site utilizes [Hugo](https://gohugo.io/), a Go-based static site generator. Follow the [instructions](https://gohugo.io/getting-started/installing/) to install Hugo on your device.

Once the theme submodule and Hugo itself are prepared, the site can be built with `hugo` while in the top-level directory.

Follow Hugo's [getting started documentation](https://gohugo.io/getting-started/) to learn more on how to use Hugo.

### Adding a new event

1. Create a new file in *content/en/events/* that follows the convention of all the other files: `year-month-day-event-name.md`
2. Copy/paste the content from another event in the same folder into your new file
3. Edit the [front matter](https://gohugo.io/content-management/front-matter/) at the top of the file to what's relevant to the event
    1. Only use tags that are already in use by other posts; Don't go creating new ones willy-nilly!
4. Edit the body contents to include accurate event venue, address, and admission information
5. Verify the content by running Hugo in [server mode](https://gohugo.io/getting-started/usage/#livereload) and checking in your browser
6. When satisfied, rebuild the site
7. Push the *public/* directory up to your web hosting environment

Did an event pass? All that's needed to move it to the *Past events* section is
to rebuild the site: The events are checked against the system clock when
built.

### Adding new music

To balance audio quality with broad support, this site makes available two different file formats:

* [Opus](https://en.wikipedia.org/wiki/Opus_(audio_format)), a newer, better-quality lossy audio format
* [MP3](https://en.wikipedia.org/wiki/Mp3), for the few remaining browsers that [cannot play Opus](https://caniuse.com/opus)

The `audio` shortcode provides simple embedding of these formats. For example:

```
{{< audio opus="/path/to/audiofile.opus" mp3="/path/to/audiofile.mp3" >}}
```

Audio [preloading](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/audio#attr-preload) defaults to `metadata`; This can be overwritten with the `preload` argument:

```
{{< audio preload="auto" opus="/path/to/audiofile.opus" mp3="/path/to/audiofile.mp3" >}}
```

Title and artist information can be added with the `title` and `artist` arguments, respectively:

```
{{< audio title="Track name" artist="John Doe" opus="/path/to/audiofile.opus" mp3="/path/to/audiofile.mp3" >}}
```

### Images

Images are organized into different paths, largely to keep track of their origins:

#### Unpublished

These directories are not published on the site but instead reside for further processing (e.g. size reduction or cropping) and will eventually be placed in static/img:

* `assets/imgs-found/` is for historical GSP images recovered from published sources like social media where no originals appear to exist.
* `assets/imgs-original/` is for fully original images in all their lossless glory.

#### Published

These directories are ready for consumption by visitors to the site and can actively be linked against when building pages:

* `img/` contains GSP-owned images, ready and available for linkage on the site. Add any new permanent images here.
* `img/poster/` contains copyrighted images from festivals, concert venues, or any other such locales that likely will not be bothered our usage of those items. To be proper, we would solicit their permission first, but...
* `img/cc/` contains [Creative Commons](https://en.wikipedia.org/wiki/Creative_Commons)-licensed works. Each of these files should contain their sources in metadata and proper attribution (if dictated by the license) given on the pages they are used.
