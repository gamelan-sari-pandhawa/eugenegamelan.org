---
title: "Listen"
aliases: ["listen-to-us"]
draft: false
menu: main
weight: 3
---

Gamelan Sari Pandhawa has performed in many venues throughout the pacific
northwest including concerts, festivals, schools, art galleries, and cultural
events. Our repertoire includes traditional and contemporary music (by both
Javanese and American composers) in a variety of styles, ranging from poetic
songs to ceremonial court repertoire.

{{< figure
    src="/thumb/2008-uo-commencement-full-ensemble.jpg"
    link="/img/2008-uo-commencement-full-ensemble.jpg"
    alt="Khendang Ageng and Ketipung resting in the grass"
>}}


## Traditional Javanese performances

The group performs two styles of traditional repertoire: a large ensemble
suitable for stage and outdoor events; and a smaller group which emphasizes the
softer, subtler instruments, appropriate for intimate settings.

{{< audio
    title="Ladrang Wilujeng"
    artist="Traditional"
    opus="/audio/Ladrang Wilujeng.opus"
    mp3="/audio/Ladrang Wilujeng.mp3"
>}}

{{< audio
    title="Gendhing Kutut Manggung"
    artist="Traditional"
    opus="/audio/Gendhing Kutut Manggung.opus"
    mp3="/audio/Gendhing Kutut Manggung.mp3"
>}}

{{< audio
    title="Tari Rejang Sutri"
    artist="Traditional"
    opus="/audio/Tari Rejang Sutri.opus"
    mp3="/audio/Tari Rejang Sutri.mp3"
>}}

### Arts Journal #920

This video features our September 2019 performance at the Atrium in Eugene, Oregon

{{< youtube id="3legyfRxsms" title="Arts Journal #920" >}}

## Modern compositions

Gamelan Sari Pandhawa often performs/premieres modern compositions for the
gamelan as well.

{{< audio
    title="Hanuman Dreams of Africa"
    artist="Qehn Jennings"
    opus="/audio/Hanuman Dreams of Africa.opus"
    mp3="/audio/Hanuman Dreams of Africa.mp3"
>}}

{{< audio
    title="In Honor of Aphrodite"
    artist="Lou Harrison"
    opus="/audio/In Honor of Aphrodite.opus"
    mp3="/audio/In Honor of Aphrodite.mp3"
>}}

{{< audio
    title="Genghing Pak Cokro"
    artist="Lou Harrison"
    opus="/audio/Gendhing Pak Cokro.opus"
    mp3="/audio/Gendhing Pak Cokro.mp3"
>}}

{{< audio
    title="Lagu Penghijauan"
    artist="K.R.T. Wasitodiningrat/Pak Cokro"
    opus="/audio/Lagu Penghijauan.opus"
    mp3="/audio/Lagu Penghijauan.mp3"
>}}

{{< audio
    title="Ketawang Subakastawa - Lancaran Tropongan"
    artist="Qehn Jennings"
    opus="/audio/Ketawang Subakastawa - Lancaran Tropongan.opus"
    mp3="/audio/Ketawang Subakastawa - Lancaran Tropongan.mp3"
>}}

## Shadow puppetry (Wayang Kulit)

{{< audio
    title="Petalon (“Overture”)"
    artist="Qehn Jennings"
    opus="/audio/Petalon - Kinanthi Juru Demung - Ladrang Gonjong Seret - Ayak-ayakan - Srepegan - Sampak.opus"
    mp3="/audio/Petalon - Kinanthi Juru Demung - Ladrang Gonjong Seret - Ayak-ayakan - Srepegan - Sampak.mp3"
>}}

