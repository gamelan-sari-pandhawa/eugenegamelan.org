---
title: "Our mission"
weight: 2
description: ""
draft: false
---

Gamelan Sari Pandhawa seeks to continue its contribution to the community
through teaching others to play gamelan, and to reach those who have never had
the privilege of experiencing this music by adding performances in different
venues throughout the area. We welcome support from community members and
institutions that value the multicultural experience.

{{< figure
    src="/thumb/2008-uo-commencement-qehn-drum.jpg"
    link="/img/2008-uo-commencement-qehn-drum.jpg"
    alt="A man plays the drums with the rest of the ensemble behind him"
>}}
