---
title: "About Gamelan Sari Pandhawa"
weight: 1
description: ""
draft: false
---

{{< figure
    src="/thumb/ensemble-in-uniform.jpg"
    link="/img/ensemble-in-uniform.jpg"
    alt="Gamelan Sari Pandhawa standing in uniform in front of the instruments"
>}}

Gamelan Sari Pandhawa is a not-for-profit 501(c)(3) organization dedicated to
providing an educational and entertaining multi-cultural experience for the
Eugene community, enriching lives through expanding cultural awareness. Since
1995, the ensemble has performed in concert and taught classes throughout the
area, and seeks to increase our community’s understanding of traditional
Javanese cultural arts, music, dance and shadow puppetry. We perform on bronze
and wood instruments elegantly handcrafted and painted by master instrument
makers in Central Java.
