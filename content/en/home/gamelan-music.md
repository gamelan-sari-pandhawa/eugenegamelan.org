---
title: "Gamelan music"
weight: 4
description: ""
draft: false
---

One of the world's most ancient and sophisticated musical traditions, gamelan
music creates a multilayered tapestry of interlocking melodies and rhythms.
Performed on bronze gongs, xylophones, drums, stringed instruments, flutes, and
voices, gamelan music has distinctive qualities that make it ideal for a
community music ensemble.

{{< figure
    src="/thumb/saron-closeup-with-hammer.jpg"
    link="/img/saron-closeup-with-hammer.jpg"
    alt="Close-up of Saron Barung with hammer laying on top"
>}}

## Accessible

Gamelan's rich textures and memorable melodies are instantly appealing and
enjoyable even to those unfamiliar with the music. At performances, members
often invite the audience to play the instruments, and many, including
children, do so with delight.

## Cooperative

By its nature, gamelan is community music - players must listen carefully and
respond to each other in a generous and unselfish way in order for the music to
be realized. All players are equally important to the group.

{{< figure
    src="/thumb/ensemble-from-above-in-square-formation.jpg"
    link="/img/ensemble-from-above-in-square-formation.jpg"
    alt="Image from above of the ensemble performing in a formation"
>}}

Gamelan Sari Pandhawa welcomes collaborations with other musicians. Our
performances often include players from other Northwest gamelans, as well as
Eugene classical musicians. The group is also dedicated to new music, and has
worked with and performed music by contemporary composers who write for
gamelan, including renowned American composer [Lou Harrison](https://wikipedia.org/wiki/Lou_Harrison).

{{< figure
    src="/thumb/overhead-of-performer.jpg"
    link="/img/overhead-of-performer.jpg"
    alt="Overhead shot of in-performance playing of a gender"
>}}

## Multi-cultural

Gamelan Sari Pandhawa strives for a deep understanding of gamelan's cultural
and historical context. Our teacher, Lewis & Clark College music professor
Widiyanto, is an 11th-generation Javanese dalang (master musician and puppet
master) who transmits to Americans the ancient yet living tradition of this
venerable art form. Members of the group are experienced in performing with
instruments from other world music traditions (balafon, koto, marimba, kora) as
well as Western music, and have used those instruments with the gamelan.

## Multi-disciplinary

Gamelan Sari Pandhawa welcomes collaborations with community artists in poetry,
dance, theater, art, video, and other media. Our musicians have accompanied
dance performances, shadow puppet plays (wayang kulit), art exhibitions, and
theatrical works.

{{< figure
    src="/thumb/ensemble-with-dancers-with-foreground-greenery.jpg"
    link="/img/ensemble-with-dancers-with-foreground-greenery.jpg"
    alt="Overhead shot of dancers in front of the sitting Gamelan performers"
>}}
