---
title: "Our history"
weight: 3
description: ""
draft: false
---

We are a Javanese gamelan performing ensemble based in Eugene, Oregon, USA.
Since our inception in 1995, we have studied with several teachers from
Indonesia, including [Ki Widiyanto](https://midiyanto.weebly.com/), who has served as our primary teacher since
before our gamelan was formed, Pak Joko Purwanto, and Pak Wayan Kantor from
Bali.

{{< figure
    src="/thumb/playing-bonang-with-foreground-greenery.jpg"
    link="/img/playing-bonang-with-foreground-greenery.jpg"
    alt="Candid shot from behind leaves of performer playing on bonangs"
>}}
