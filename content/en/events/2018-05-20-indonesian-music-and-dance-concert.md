---
title: "Indonesian Music and Dance Concert"
date: 2018-05-20T15:00:00-0800
draft: false
tags: ["eugene", "concert"]
---

**Episcopal Church of the Resurrection**

**3925 Hilyard St, Eugene, OR 97405**

**Free admission (but donations welcome)**

Gamelan Sari Pandhawa will present a concert at the [Episcopal Church of the Resurrection](https://resurrectioneugene.org/) in the afternoon.

The concert will include a mixture of contemporary and traditional Javanese pieces, as well as original works by our Music Director, [Ken Jennings](http://members.efn.org/~qehn/), and gamelan interpretations of western classical pieces.

Special guests include the Tirta Tari Dance ensemble from Lane Community College, and two Indonesian-American teens who have been studying the traditional dance of Java.

We invite you to join us this Sunday and enjoy the beautiful sound of the gamelan. There is a suggested donation of $5, but no one will be turned away for lack of funds.
