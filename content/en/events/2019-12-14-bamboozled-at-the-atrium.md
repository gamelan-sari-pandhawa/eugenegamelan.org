---
title: "Bamboozled at The Atrium"
date: 2019-12-14T14:00:00-0800
draft: false
tags: ["concert", "eugene", "rindik"]
---

**The Atrium**

**99 W 10th Ave, Eugene, OR 97401**

**Free admission**

Please join us for an informal concert of music of (mostly) bamboo instruments from Indonesia. Bamboozled is a subsidiary of Gamelan Sari Pandhawa. Modeled on the rindik ensembles of Bali, with the addition of instruments from other regions of Indonesia, Bamboozled plays traditional and contemporary Indonesian music in bright, upbeat arrangements. This unique concert is not to be missed!

![Bamboozled instruments](/img/bamboo-instruments.jpg)
