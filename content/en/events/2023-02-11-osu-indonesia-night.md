---
title: "OSU Indonesia Night 2023"
date: 2023-02-11T17:15:00-0800
draft: false
tags: ["corvallis", "festival", "rindik"]
---

**2501 SW Jefferson Way**

**Corvallis, 97331**

**Free admission**

*Bamboozled*, Gamelan Sari Pandhawa's bamboo-based sister project, is proud to perform at Oregon State University's 2023 Indonesia Night! This event is organized by [Persatuan Mahasiswa Indonesia Amerika Serikat](https://wikipedia.org/wiki/Permias) (*Organization of the Indonesian Students in the United States*, abbreviated as PERMIAS). The festival will be held at Oregon State University's Corvallis location. The event will feature a variety of Indonesian dance and music that you won't want to miss.

![Bamboozled instruments](/img/bamboo-instruments.jpg)
