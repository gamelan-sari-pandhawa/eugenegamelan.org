---
title: "Park Blocks"
date: 2017-08-16T12:00:00-0700
draft: false
tags: ["concert", "eugene"]
---

**Eugene Park Blocks**

**Free admission**

We will perform on the [Park blocks](https://www.eugene-or.gov/3257/Park-Blocks) (Where the Saturday Market usually resides).
