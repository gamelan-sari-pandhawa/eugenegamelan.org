---
title: "Asian Celebration 2018"
date: 2018-02-18T11:30:00-0800
draft: false
tags: ["eugene", "festival"]
---

**Lane Events Center**

**796 W 13th Ave, Eugene, OR 97402**

**Adults $6/day, $10 both days, 12 & under free**

Come and join us at the Main Stage of the [Asian Celebration](https://asiancelebration.org/) in Eugene, Oregon. We'll be performing with the *Tirta Tari* dancers from Lane Community College. Also performing will be Gamelan Generasi Penerus (children's gamelan).

Asian Celebration is a weekend of Asian cultural performances, music, and fine art; demonstrations of Asian crafts, cooking, and martial arts; a heritage exhibit, an Asian food court and marketplace, special events, and more. Visit their website for more information.

![2018 Asian Celebration poster](/img/poster/2018-asian-celebration-poster.jpg)
