---
title: "Asian Celebration 2020"
date: 2020-02-16T11:30:00-0800
draft: false
tags: ["eugene", "festival"]
---

**Lane Events Center**

**796 W 13th Ave, Eugene, OR 97402**

**Free admission**

Come and join us at the Main Stage of the [Asian Celebration](https://asiancelebration.org/) in Eugene, Oregon. We will play traditional and contemporary music for Javanese gamelan. We will be joined by our young people's auxiliary, Gamelan Generasi Penerus - Gamelan: the Next Generation.

Asian Celebration is a weekend of Asian cultural performances, music, and fine art; demonstrations of Asian crafts, cooking, and martial arts; a heritage exhibit, an Asian food court and marketplace, special events, and more. Visit their website for more information.

![Asian Celebration 2020](/img/poster/2020-asian-celebration-poster.jpg)
