---
title: "Salem World Beat Festival 2023"
date: 2023-06-24T10:00:00-0700
tags: ["festival", "salem"]
---

**Riverfront Park**

**200 Water St NE, Salem, OR 97301**

**Admission fee: $10 per person - 14 and younger/SNAP card holders free!**

![World Beat Logo](/img/poster/World-Beat-Logo_700.png)

Come and join us at Salem World Beat's annual festival! Gamelan Sari Pandhawa
will be featured, performing traditional and contemporary music for Javanese
Gamelan.

World Beat boasts a weekend of food, music, dance, and other cultural events
from over 70 nations and cultures world-wide.

More information about the event can be found on [Salem's Multicultural website](https://www.salemmulticultural.org/festival/world-beat-festival).

{{<figure
    src="/img/cc/world-beat-2010-indian-dancer-thumb.webp"
    link="/img/cc/world-beat-2010-indian-dancer.webp"
    alt="A dancer donning white garb and an ornate, feathered headdress twirls with eyes closed."
    caption="Image: [Glen Bledsoe](https://www.flickr.com/photos/glenbledsoe/4737306293/), CC BY 2.0"
>}}
