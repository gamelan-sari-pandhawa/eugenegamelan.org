---
title: "The Quest: A Multimedia Wayang Kulit"
date: 2018-08-30T21:00:00-0700
draft: false
tags: ["concert", "eugene"]
---

**Kesey Square**

**Corner of Broadway and Willamette, Downtown Eugene, OR**

**Free admission**

In a collaboration with video artist [Terry Holloway](https://terryholloway.com/), Gamelan Sari Pandhawa will present a unique theatrical event. *The Quest* is a fusion of gamelan music, Javanese Wayang Kulit (shadow puppet theater), and digital media. The traditional shadows cast by the intricately carved Indonesian puppets are enhanced by video projections of images, colors, and textures that are coordinated with the changing scenes of the puppet drama. Please join us for this new and exciting presentation.

![Wayang Kulit](/img/wayang.jpg)
