---
title: "Asian Celebration 2021"
date: 2021-07-31T21:00:00-0700
draft: false
tags: ["eugene", "festival"]
---

**Alton Baker Park**

**632 Day Island Rd, Eugene, OR 97401**

**Free admission**

Gamelan Sari Pandhawa is proud to once again feature in Eugene's [Asian Celebration](https://asiancelebration.org/).

Come and join us at the Main Stage of the [Asian Celebration](https://asiancelebration.org/) in Eugene, Oregon. We will play traditional and contemporary music for Javanese gamelan. We will be joined by our young people's auxiliary, Gamelan Generasi Penerus - Gamelan: the Next Generation.

Asian Celebration is a weekend of Asian cultural performances, music, and fine art; demonstrations of Asian crafts, cooking, and martial arts; a heritage exhibit, an Asian food court and marketplace, special events, and more. Visit their website for more information.

![Asian Celebration 2021](/img/poster/2021-asian-celebration-poster.jpg)
