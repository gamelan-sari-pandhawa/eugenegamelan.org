---
title: "Asian Celebration 2024"
date: 2024-07-27T11:05:00-0700
draft: false
tags: ["eugene", "festival"]
---

**Alton Baker Park**

**632 Day Island Rd, Eugene, OR 97401**

**Free admission**

Gamelan Sari Pandhawa is proud to once again feature in Eugene's [Asian Celebration](https://asiancelebration.org/).

Come and join us at the Main Stage of the festival in Eugene, Oregon. We will play traditional and contemporary music for Javanese gamelan.

Asian Celebration is a weekend of Asian cultural performances, music, and fine art; demonstrations of Asian crafts, cooking, and martial arts; a heritage exhibit, an Asian food court and marketplace, special events, and more. Visit their website for more information.

![Asian Celebration 2024](/img/poster/2024-asian-celebration-poster.jpg)
