---
title: "The Quest: A Multimedia Wayang Kulit"
date: 2019-01-26T18:30:00-0800
draft: false
tags: ["concert", "eugene"]
---

**Buddha Eye Zen Community Temple**

**2190 Garfield, Eugene, OR**

**$10 admission fee**

In a collaboration with video artist [Terry Holloway](https://terryholloway.com/), Gamelan Sari Pandhawa will present a unique theatrical event. *The Quest* is a fusion of gamelan music, Javanese Wayang Kulit (shadow puppet theater), and digital media. The traditional shadows cast by the intricately carved Indonesian puppets are enhanced by video projections of img, colors, and textures that are coordinated with the changing scenes of the puppet drama. Please join us for this new and exciting presentation.

This event will take place at [Buddha Eye Zen Community Temple](https://www.buddhaeye.org/), for which it serves as a fund raiser. Buddha Eye has been very supportive of Gamelan Sari Pandhawa, and we encourage you to support them now.
