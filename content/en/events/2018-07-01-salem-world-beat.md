---
title: "Salem World Beat 2018"
date: 2018-06-30T13:30:00-0700
draft: false
tags: ["festival", "salem"]
---

**Riverfront Park**

**200 Water St NE, Salem, OR 97301**

**Free admission**

Come and join us at the Salem World Beat at the Riverfront Park in Salem, Oregon. We will be playing traditional and contemporary music for Javanese Gamelan.

[World Beat](https://www.salemmulticultural.org/festival/world-beat-festival) is a weekend of food, music, and cultural events and displays.
