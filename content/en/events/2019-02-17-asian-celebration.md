---
title: "Asian Celebration 2019"
date: 2019-02-17T11:30:00-0800
draft: false
tags: ["eugene", "festival"]
---

**Lane Events Center**

**796 W 13th Ave, Eugene, OR 97402**

**Adults $6/day, $10 both days, 12 & under free**

Come and join us at the Main Stage of the [Asian Celebration](https://asiancelebration.org/) in Eugene, Oregon. We'll be joined by Bonnie Simoa and the tirta Tari dancers from Lane Community College, and by Gamelan Generasi Penerus, GSP's young people's auxiliary, who will be playing Balinese music on bamboo instruments.

Asian Celebration is a weekend of Asian cultural performances, music, and fine art; demonstrations of Asian crafts, cooking, and martial arts; a heritage exhibit, an Asian food court and marketplace, special events, and more. Visit their website for more information.

![Asian Celebration 2019](/img/poster/2019-asian-celebration-poster.jpg)
