---
title: "Book us"
draft: false
aliases: ["book"]
menu: main
weight: 1
---

{{< figure
    src="/thumb/live-performance.jpg"
    link="/img/live-performance.jpg"
    alt="The ensemble performing live in front of stained glass"
>}}

For prices and performance bookings, please email us and we'll respond as soon
as we can.

{{< cloakemail address="booking@eugenegamelan.org" >}}

## We play for any occasion

Please consider booking Eugene's Gamelan Sari Pandhawa for your performance
venue, exhibition opening, festival, ceremonial gatherings, or any other event
that can be enhanced by this spellbinding music. We have performed at weddings,
memorial services, and private parties.

We have performed throughout Oregon, including the [Eugene Celebration](https://lanemusichistory.com/eugene-celebration/), [Oregon Coast Music Festival](https://www.oregoncoastmusic.org/), [Asian Celebration](https://asiancelebration.org/), [Salem World Beat Festival](https://www.salemmulticultural.org/festival/world-beat-festival), [Willamette Valley Folk Festival](https://lanemusichistory.com/willamette-valley-folk-festival/), University of Oregon's Beall Hall and Erb Memorial Union,
and more, both in concert and accompanying dance and shadow puppet theater. The
ensemble has also performed at schools, art galleries, and cultural events.

{{< figure
    src="/img/purwanto-demonstrating-kendhang.jpg"
    link="/img/purwanto-demonstrating-kendhang.jpg"
    alt="A performer demonstrates the Kendhang and Ketipung to onlooking children"
>}}

Our repertoire includes traditional and contemporary music in a variety of
styles, ranging from poetic songs to ceremonial court repertoire; From quiet,
meditative pieces to stirring battle music.

Musicians observe traditional playing style: seated on the floor, wearing
colorful Indonesian [batik](https://en.wikipedia.org/wiki/Batik) costumes. The
beauty of the teak and bronze instruments, hand-crafted in Java, complements
their ravishing sound.

{{< figure
    src="/thumb/2022-07-30-asian-celebration-waiting-on-stage.jpg"
    link="/img/2022-07-30-asian-celebration-waiting-on-stage.jpg"
    alt="A puppetmaster performs behind the stage while the gamelan performs"
>}}

## We're flexible

The group performs in either of two configurations: a large ensemble suitable
for stage and outdoor events; and a smaller group which emphasizes the softer,
subtler instruments, appropriate for intimate settings. We also collaborate
with dancers and other performers.

## Not just music

In addition to purely musical pieces, the ensemble performs [Wayang
kulit](https://en.wikipedia.org/wiki/Wayang_kulit), the fabled Javanese shadow
puppet theater, accompanied by gamelan music. The shadow puppet plays we
perform are based on stories from the
[Ramayana](https://en.wikipedia.org/wiki/Ramayana),
[Mahabharata](https://en.wikipedia.org/wiki/Mahabharata), and other world
literatures (e.g. [Beowulf](https://en.wikipedia.org/wiki/Beowulf)).

{{< figure
    src="/img/school-assembly-wayang-kulit.jpg"
    link="/img/school-assembly-wayang-kulit.jpg"
    alt="A puppetmaster performs behind the stage while the gamelan performs"
>}}
