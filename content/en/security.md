---
title: "Security Vulnerability Disclosure"
draft: false
---

Thank you for your interest in improving our security!

Please note that we are a non-profit organization and cannot offer competitive rewards. We'll try our best to accomodate compensation but the only thing we can guarantee is our eternal gratitude for keeping our lights on.

Thank you for your responsible disclosure!

{{< cloakemail address="disclosure@eugenegamelan.org" >}}
